# Solving Polynomial Initial Value Problem (PIVP) in polynomial time

This is an implementation, in Maple, of the algorithm from [^1]. This is not a Maple library,
thus at the moment it is a bit clumsy to use. The goal was mostly to experiment and see how it
behaves in pratice. Since the algorithm is mostly theoretical (showing that there exists a polynomial-time
algorithm), it is not particularly optimized toward practical use. Note that the key features of this
algorithm are:
- it is always correct: it returns the desired value up to the request precision
- it has runtime polynomial in several parameters (see the paper), but mostly in the requested precision
  and the length of the solution curve up to the requested time.


[^1]: Computational complexity of solving polynomial differential equations over unbounded domains
    
    Olivier Bournez, Daniel Silva Graça and Amaury Pouly

    Theoretical Computer Science (TCS), 626:67-82, 2016    

