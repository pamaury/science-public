# Public research code and documents

This repository contains various pieces of codes or document related to my research papers.

## Solving Polynomial Initial Value Problems (PIVP)

In [^1], we developed an algorithm some some polynomial ordinary different equations, also
known as polynomial initial values problems (PIVP). This algorithm runs in polynomial time
in the length of the curve. Also the goal was mostly theoretical (showing that there exists
a polynomial-time algorithm), I implemented in Maple to experiment with it. The code can
be found [here](pivp_solving/)

## PIVP to Polynomial DAE

In [^2], the authors show that any polynomial ordinary differential equation can be rewritten
as polynomial differential algebraic equation. The proof involves computing Grobner basis and
doing variable elimination. Out of curiosity and for experimention, I implement the procedure
described in this paper in Maple. Of course since computing Grobner basis can be doubly-exponential
in time, it only produces useful results for very small systems. The code can be found
[here](pivp_to_dae/).



[^1]: Computational complexity of solving polynomial differential equations over unbounded domains
    
    Olivier Bournez, Daniel Silva Graça and Amaury Pouly

    Theoretical Computer Science (TCS), 626:67-82, 2016

[^2]: Some properties of solutions to polynomial systems of differential equations

    Carothers, D. C. and Parker, G. E. and Sochacki, J. S. and Warne, P. G. 

    Electron. J. Diff. Eqns., 40, 2005
